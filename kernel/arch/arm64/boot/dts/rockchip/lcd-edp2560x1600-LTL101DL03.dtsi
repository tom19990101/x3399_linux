/*
 * Copyright (C) 2019 9tripod, Inc.
 * arch/arm64/boot/dts/rockchip/lcd-edp2560x1600-LTL101DL03.dtsi
 * lcd model: ltl101dl03
 * resolution: 2560x1600
 */

/ {
	edp_panel: edp-panel {
		compatible = "samsung,ltl101dl03", "panel-simple";
		backlight = <&backlight>;
		power-supply = <&vcc3v3_s0>;
		enable-gpios = <&gpio4 30 GPIO_ACTIVE_HIGH>;

		ports {
			panel_in_edp: endpoint {
				remote-endpoint = <&edp_out_panel>;
			};
		};
	};
};

&edp {
	status = "okay";

	ports {
		edp_out: port@1 {
			reg = <1>;
			#address-cells = <1>;
			#size-cells = <0>;

			edp_out_panel: endpoint@0 {
				reg = <0>;
				remote-endpoint = <&panel_in_edp>;
			};
		};
	};
};

&edp_in_vopl {
	status = "disabled";
};

&route_edp {
	status = "okay";
};

&i2c1 {
	status = "okay";

	gt9xx: gt9xx@14 {
		compatible = "goodix,gt9xx";
		reg = <0x14>;
		touch-gpio = <&gpio1 20 IRQ_TYPE_EDGE_RISING>;
		reset-gpio = <&gpio1 9 GPIO_ACTIVE_LOW>;
		max-x = <2560>;
		max-y = <1600>;
		tp-size = <101>;
	};
};
